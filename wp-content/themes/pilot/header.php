<!DOCTYPE html>
	<?php 
		global $pilot;
		get_template_part( 'views/head' ); 
	?>
	<body <?php body_class(); ?> id="body_id">
		<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KW896MG"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
		<div class="site">
			<?php get_template_part( 'views/header' ); ?>
			<div class="site-content">
				<main class="site-main">