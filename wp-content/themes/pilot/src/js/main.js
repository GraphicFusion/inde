function twoUp(){
	var width = $(window).width();
	if(width > 1260 ){
		$.each( $('.block-twoup .row'), function(k,v){
			var rowHeight = 0;
			$.each( $(v).find('.twoup-foot-content'), function(a,b){
				var contentHeight = $(b).outerHeight();
				console.log(contentHeight, rowHeight);
				if(contentHeight > rowHeight){
					rowHeight = contentHeight;
				}
			})
			$(v).find('.twoup-foot-content').height(rowHeight);
		});
	}
	else{
		$('.twoup-foot-content').height('auto');

	}
	// set image hieght
	$.each( $('.block-twoup .shadow-card .img-block-wrap'), function(k,v){
		var width = $(v).width(),
			height = width * 2/3;
			$(v).height(height);
			$(v).find('.bg-image').height(height);
	})
}
function gallery(){
	$.each( $('.block-gallery .img-block-wrap'), function(k,v){
	var width = $(window).width();
		if(width > 1260 ){
			$(v).height('600px');
		}
		else{ 
			var width = $(v).width(),
				height = width * 2/3;
				$(v).height(height);
				$(v).find('video').height(height);
		}
	})

}

function threeUpSlider(){
	var width = $(window).width();
	if(width < 1260 ){ 
		$('.block-card .slide-wrapper').slick({
			arrows:false,
			centerMode:true,
			variableWidth:true
		});
		var footerPos = $('.site-footer .mobile-menu ').offset();
		//$('.row.slick-slider').css('padding-left',footerPos.left);
	}
	else{
		$('.block-card .slide-wrapper').slick({
			variableWidth:true,
			slidesToShow:3
		});

	}
	$.each( $('.block-card'), function(k,v){
		var rowHeight = 0;
		$.each( $(v).find('.card-foot-content'), function(a,b){
			var contentHeight = $(b).outerHeight();
			console.log(contentHeight, rowHeight);
			if(contentHeight > rowHeight){
				rowHeight = contentHeight;
			}
		})
		$(v).find('.card-foot-content').height(rowHeight);
	});

	$.each($('.shadow-card .img-block-wrap'), function(k,v){
		var width = $(v).width(),
			height = width * 2/3;
			$(v).height(height);
			$(v).find('.bg-image').height(height);
	})
}
function setFooterDrawdown(){
	var lastModule = $('article .module').last();
	console.log('lastmodule:',$('article .module'));
	if( lastModule.hasClass('block-card') || lastModule.hasClass('block-action') || 
	 lastModule.hasClass('block-quote') ||  lastModule.hasClass('block-twoup') || 
		 lastModule.hasClass('block-teaser') ||  lastModule.hasClass('block-gallery') ||   lastModule.hasClass('block-track') || 
		   lastModule.hasClass('block-insta') ||   lastModule.hasClass('block-team') || 
		     lastModule.hasClass('block-membership') ){
		console.log('footer drawdonw:', $('article .module').last());
				var height = ($('article .module').last().height())/2;
				$('footer.site-footer').css('padding-top',height).css('margin-top',-height);
			}
}
function setupAccordion(){
	var allPanels = $('.accordion > dd').hide(),
	allPanelLabels = $('.accordion > dt');
	
	$('.accordion > dt .acc-tog').click(function() {
		if ( !$(this).closest('dt').hasClass('active js-open') ) {
			//allPanels.slideUp();
			//allPanelLabels.removeClass('active js-open');
			$(this).closest('dt').next().slideDown();
			$(this).closest('dt').addClass('active js-open');
		}
		else {
			$(this).closest('dt').removeClass('active js-open').next().slideUp();
		}
		return false;
	});
}

function cleanUp(){
	twoUp();
	threeUpSlider();
	gallery();		
	setFooterDrawdown();	
}
function removeMobileVideo(){
	var width = $(window).width();
	if(width < 1260 ){ 
		$.each($('video.bg-vid'),function(k,v){
			$(v).removeAttr('src');
			var poster = $(v).attr('poster');
			console.log('poster',poster);
			$(v).parent().css('background-image','url(' + poster + ')');
			$(v).remove();
			console.log( 			$(v).parent() );
			//$(this).attr('src','');
			console.log('video',$(v)[0]);
		})
	}
}
function getScrollBarWidth () {
	    var $outer = $('<div>').css({visibility: 'hidden', width: 100, overflow: 'scroll'}).appendTo('body'),
	        widthWithScroll = $('<div>').css({width: '100%'}).appendTo($outer).outerWidth();
	    $outer.remove();
	    return 100 - widthWithScroll;
	};
	window.addEventListener("load",function() {
	// Set a timeout...
	setTimeout(function(){
		// Hide the address bar!
		window.scrollTo(0, 1);
	}, 0);
});
jQuery(document).ready(function($) {
			var sb = getScrollBarWidth(),
				winWidth = $(window).width();
			maxWidth = parseInt(winWidth - sb);
			$('.block-navigation .mobile-and-footer').css('max-width',maxWidth);
	$('.menu-button,.menu-button-close').click(function(){
		if( $('body').hasClass('menu-scroll') ){
//			$('.block-navigation').removeClass('open-mobile');
			$('body').addClass('show-reveal');
//			$('body').removeClass('open-mobile');
			$('body').removeClass('menu-scroll').removeClass('stuck-header');
			$('.site-content').show();
			$('.img-block-wrap').width(window.blockWidth);
			//$('body').css('height','100%');
			window.scrollTo(0,window.scrollPosition);
			cleanUp();
		}
		else{
			window.blockWidth = $('.img-block-wrap').width();
			//$('.block-navigation').addClass('open-mobile');
			//$('body').addClass('open-mobile');
			$('body').addClass('menu-scroll');
			var scrollPosition = $(document).scrollTop();
			window.scrollPosition = scrollPosition;
			var footerHeight = $('.block-footer .footer-content').height(),
				bodyHeight = document.body.scrollHeight,
				scrollToHeight = parseInt(bodyHeight - footerHeight);
			window.scrollTo(0,0);
			$('.site-content').hide();
			$('body').removeClass('show-reveal').addClass('stuck-header');
		}
	});
	var $footer_menu = $(".mobile-navigation").clone().appendTo( $('.footer-content') );
	$footer_menu.find('.menu-close-wrapper-close').remove();
	$footer_menu.find('.container-fluid.container-lg').removeClass('container-card');
	setupAccordion();
	cleanUp();
	removeMobileVideo();

	$(window).on('resize', function(){
		cleanUp();
	});
	var firstBlock = $('.entry-content .module:eq(1)').first();
	if( $(firstBlock).hasClass('block-card') || $(firstBlock).hasClass('block-twoup') ){
		$(firstBlock).css('margin-top','-30px');
		if( $(firstBlock).hasClass('block-card') ){
			$(firstBlock).find('.slick-list').css('padding-top','0 !important');
		}
	}
	document.addEventListener('scroll', function (event) {
		if(!$('body').hasClass('menu-scroll')){
			var headerTrigger = 150,
				footerTrigger = 600,
				ydistance = $(window).scrollTop();
			if(ydistance > headerTrigger){
				$('body').addClass('stuck-header');
				var height = $('.navigation').outerHeight();
			}
			else{
				$('body').removeClass('stuck-header');
				var height = maxHeight;
			}
			if(ydistance > footerTrigger){
				$('body').addClass('show-reveal');			
			}
			else{
				$('body').removeClass('show-reveal');			
			}
			$('body').css('padding-top',height);
		}
	}, true /*Capture event*/);

});