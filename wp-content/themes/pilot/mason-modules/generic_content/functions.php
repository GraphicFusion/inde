<?php
	function build_generic_content_layout(){
		$args = array(
			'isHeader' => mason_get_sub_field('generic_content_block_isHeader'),
			'title' => mason_get_sub_field('generic_content_block_title'),
			'use_card' => mason_get_sub_field('generic_content_block_use_card'),
			'content' => mason_get_sub_field('generic_content_block_content'),
		);
		$args['module_styles'] = [];
		if(get_sub_field('generic_content_block_margin-top')){
			$args['module_styles']['margin-top'] = get_sub_field('generic_content_block_margin-top');
		}
		if(get_sub_field('generic_content_block_z-index')){
			$args['module_styles']['z-index'] = get_sub_field('generic_content_block_z-index');
		}		
		if(get_sub_field('generic_content_block_margin-bottom')){
			$args['module_styles']['margin-bottom'] = get_sub_field('generic_content_block_margin-bottom');
		}

		return $args;
	}
?>