<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */
	global $args;
?>
	<?php
	if($args['image']){
		$icon = $args['image']['url'];
	}
	else{
	}
?>
<div class="container-fluid container-md container-sm">
	<div class="row">
		<div class="col-lg-12">
			<div class="gc-content">
				<?php if($args['title']) : ?>
					<div class="title"><?php echo $args['title']; ?></div>
				<?php endif; ?>
				<?php if($args['features']) : ?>
					<div class="list-wrapper">
					<?php foreach($args['features'] as $arr) : ?>

						<div class="list-item"><img class="icon" src="<?php echo $icon; ?>"><h6 class="li-content"><?php echo $arr['feature']; ?></h6></div>
					<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>