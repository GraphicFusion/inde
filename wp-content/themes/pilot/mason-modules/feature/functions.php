<?php
	function build_feature_layout(){
		$args = array(
			'title' => mason_get_sub_field('feature_block_title'),
			'image' => mason_get_sub_field('feature_block_image'),
			'features' => mason_get_sub_field('feature_block_features'),
		);
		$args['module_styles'] = [];
		if(get_sub_field('feature_block_margin-top')){
			$args['module_styles']['margin-top'] = get_sub_field('feature_block_margin-top');
		}
		if(get_sub_field('feature_block_z-index')){
			$args['module_styles']['z-index'] = get_sub_field('feature_block_z-index');
		}
		if(get_sub_field('feature_block_margin-bottom')){
			$args['module_styles']['margin-bottom'] = get_sub_field('feature_block_margin-bottom');
		}

		return $args;
	}
?>