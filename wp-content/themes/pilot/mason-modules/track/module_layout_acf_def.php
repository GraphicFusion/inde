<?php
global $pilot;
// add module layout to flexible content


    // add module layout to flexible content 
    $name = "track";
    $module_layout = array (
        'key' => create_key($name, 'block'),
        'name' => 'track_block',
        'label' => 'Track Configurations',
        'display' => 'block',
        'sub_fields' => array (
                    array(
                        'key' => create_key('track','image'),
                        'label' => 'Background Image' ,
                        'name' => 'track_block_image',
                        'type' => 'image',
                        'instructions' => '',
                        'required' => 0,
                        'wrapper' => array(
                            'width' => '30%',
                            'class' => "",
                            'id' => '',
                        ),
                        'return_format' => 'array',
                        'preview_size' => 'thumbnail',
                        'library' => 'all',
                        'min_width' => '',
                        'min_height' => '',
                        'min_size' => '',
                        'max_width' => '',
                        'max_height' => '',
                        'max_size' => '',
                        'mime_types' => '',
                    ),
                        array(
                'key' => create_key($name,'overlay_opacity'),
                'label' => 'Opacity',
                'name' => $name.'_block_overlay_opacity',
                'type' => 'number',
                'instructions' => 'Set from 0 to 1 (for example 0.75)',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => create_key($name,'modify'),
                            'operator' => '==',
                            'value' => '1',
                        ),
                    ),
                ),
                'wrapper' => array(
                    'width' => 35,
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'min' => 0,
                'max' => 1,
                'step' => '.01',
                'readonly' => 0,
                'disabled' => 0,
            ),
            array(
                'key' => create_key($name,'overlay_color'),
                'label' => 'Color',
                'name' => $name.'_block_overlay_color',
                'type' => 'color_picker',
                'instructions' => 'Set the color overlay for the image or video',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => create_key($name,'modify'),
                            'operator' => '==',
                            'value' => '1',
                        ),
                    ),
                ),
                'wrapper' => array(
                    'width' => 35,
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
            ),

            array (
                'key' => create_key($name,'rows'),
                'label' => 'Rows',
                'name' => 'track_block_rows',
                'type' => 'repeater',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'collapsed' => '',
                'min' => '',
                'max' => '',
                'layout' => 'block',
                'button_label' => 'Add A Track Configuration',
                'sub_fields' => array (
                    array(
                        'key' => create_key($name,'image'),
                        'label' => 'Slide Image' ,
                        'name' => 'track_block_slide_image',
                        'type' => 'image',
                        'instructions' => '',
                        'required' => 0,
                        'wrapper' => array(
                            'width' => '30%',
                            'class' => "",
                            'id' => '',
                        ),
                        'return_format' => 'array',
                        'preview_size' => 'thumbnail',
                        'library' => 'all',
                        'min_width' => '',
                        'min_height' => '',
                        'min_size' => '',
                        'max_width' => '',
                        'max_height' => '',
                        'max_size' => '',
                        'mime_types' => '',
                    ),

                    array (
                        'key' => create_key($name,'track'),
                        'label' => 'Title',
                        'name' => 'track_block_track',
                        'type' => 'textarea',
                        'instructions' => '',
                        'required' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => "",
                            'id' => '',
                        ),
                        'default_value' => '',
                        'tabs' => 'all',
                        'toolbar' => 'full',
                        'track_upload' => 1,
                    ),
                    array (
                        'key' => create_key('track','content'),
                        'label' => 'Caption' ,
                        'name' => 'track_block_content',
                        'type' => 'wysiwyg',
                        'instructions' => '',
                        'required' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => "",
                            'id' => '',
                        ),
                        'default_value' => '',
                        'tabs' => 'all',
                        'toolbar' => 'full',
                        'track_upload' => 1,
                    ),
                    array(
                        'key' => create_key($name,'pdf'),
                        'label' => 'Downloadable PDF' ,
                        'name' => 'track_block_slide_pdf',
                        'type' => 'image',
                        'instructions' => '',
                        'required' => 0,
                        'wrapper' => array(
                            'width' => '30%',
                            'class' => "",
                            'id' => '',
                        ),
                        'return_format' => 'array',
                        'preview_size' => 'thumbnail',
                        'library' => 'all',
                        'min_width' => '',
                        'min_height' => '',
                        'min_size' => '',
                        'max_width' => '',
                        'max_height' => '',
                        'max_size' => '',
                        'mime_types' => '',
                    ),

                ),

            ),
        ),);

?>