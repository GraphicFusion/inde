<?php 
	global $args; 
?>
<div class=" container-fluid container-lg container-md container-sm ">
	<div class="card"  style="background-image: url(' <?php echo $args['bg_image_url']; ?>');">
		<div class="img-overlay"> 
			<div class="filter" 
			style="background-image:linear-gradient(to right, <?php echo $args['overlay_color']; ?> 100%, transparent ); opacity: <?php echo $args['overlay_opacity']; ?>;"></div>
		</div>
					<div class="shadow-track">

<?php
	if(count($args['tracks']) > 0 ) :
		$i = 0;
		foreach($args['tracks'] as $track):  ?>
				<div class="track-foot-content row">
						<div class="col-lg-6 img-wrap">
							<?php if( $track['img']  ) : ?>
								<img src="<?php echo $track['img']['url']; ?>" class="img">
							<?php endif; ?>
						</div>
						<div class="col-lg-6 content-wrap">
								<div class="content">
									<?php if( $track['title']  ) : ?>
										<h3><?php echo $track['title']; ?></h3>
									<?php endif; ?>
									<?php if(  $track['caption']  ) : ?>
										<div class="cap-wrapper">
											<?php echo $track['caption']; ?>
										</div>
									<?php endif; ?>
									<?php if(  $track['download']  ) : ?>
										<a download class="inde-btn" href="<?php echo $track['download']['url']; ?>">DOWNLOAD PDF</a>
									<?php endif; ?>									
								</div>
						</div><!--/title-wrap-->
				</div><!--/track-foot-content-->
		<?php endforeach; ?>
	<?php endif; ?>
					</div>
						<div class="nav-wrapper-wrapper">
							<div class="nav-wrapper"></div>
						</div>
				</div><!--/shadow-track-->
			</div>
<script>
	$( document ).ready(function() {
		$('#<?php echo $args['id']; ?> .shadow-track').on('init', function(event, slick, direction){
			var width = $(window).width();
			if(width > 1260 ){ 
	  			var maxHeight = 0;
	  			$.each( $('#<?php echo $args['id']; ?> .shadow-track .img-wrap img') , function(k,v){
	  				if( $(v).height() > maxHeight ){
	  					maxHeight = $(v).height();
	  				}
	  			})
	  			$('#<?php echo $args['id']; ?> .shadow-track .content').height(maxHeight);
	  		}
	  		else{
				var contentPos = $('#<?php echo $args['id']; ?> .track-foot-content.slick-current .content').position();
				console.log('contentPos',contentPos.right);
				var target = $('#<?php echo $args['id']; ?> .track-foot-content.slick-current .content').offset();
				console.log('target',target);

			}
		});
		$('#<?php echo $args['id']; ?> .shadow-track').slick({
			slidesToShow:1,
			dots:false,
			arrows:true,
			appendArrows: $('#<?php echo $args['id']; ?> .nav-wrapper'),
		});


	});
</script>