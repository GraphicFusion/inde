<?php
	function build_track_layout(){
		global $i;
		$args = [];
		$tracks = [];
		$rows_arr = mason_get_sub_field('track_block_rows');
		$image = mason_get_sub_field('track_block_image');
		$args['overlay_color'] = mason_get_sub_field('track_block_overlay_color');
		$args['overlay_opacity'] = mason_get_sub_field('track_block_overlay_opacity');
		if( is_array( $image ) ){
			$args['bg_image_url'] = $image['url'];
		}
		if( is_array($rows_arr) ){
			$rows = array();
			foreach( $rows_arr as $row_arr){
				$track_args = array(
					'title' => $row_arr['track_block_track'],
					'img' => $row_arr['track_block_slide_image'],
					'id' => 'track_block'.'_'.$i,
					'caption' => $row_arr['track_block_content'],
					'download' => $row_arr['track_block_slide_pdf'],
				);
				$tracks[] = $track_args;
			}
		}
		$args['tracks'] = $tracks;
		$args['module_styles'] = [];
		if(get_sub_field('track_block_margin-top')){
			$args['module_styles']['margin-top'] = get_sub_field('track_block_margin-top');
		}
		if(get_sub_field('track_block_margin-bottom')){
			$args['module_styles']['margin-bottom'] = get_sub_field('track_block_margin-bottom');
		}
		
		if( is_array( $args ) ){
			return $args;
		}
	}
	function track_admin_enqueue($hook) {
		wp_register_style( 'track_wp_admin_css', get_template_directory_uri() . '/mason-modules/track/admin-style.css', false, '1.0.0' );
        wp_enqueue_style( 'track_wp_admin_css' );
    }
	add_action( 'admin_enqueue_scripts', 'track_admin_enqueue' );
?>