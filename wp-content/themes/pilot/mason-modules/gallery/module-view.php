<?php
    global $args;
    $block_name = 'gallery_block_';   
    $block_id = $args['acf_incr'];
    unset($args['acf_incr']);
    if(!$args['skip_header']): 
?>
<div class=" container-fluid container-lg container-md container-sm ">
    <div id="slick-slider-<?php echo $block_id; ?>" style="">
        <?php foreach ( $args['slides'] as $arg ) : $slide_id = $arg[$block_name . "slide_id"]; ?>
            <?php 
                $video = "";
                if(  $arg[$block_name . 'video_file_mp4'] ||  $arg[$block_name . 'youtube'] ){
                    $video_type = 'video/mp4';
                    $controls = "false";
                    if($arg[$block_name . 'youtube']){
                        $video_type = 'youtube';
                        $controls = "true";
                    }
                    $video = ($arg[$block_name . 'youtube'] ? $arg[$block_name . 'youtube'] : $arg[$block_name . 'video_file_mp4']['url'] );
                }
                $bg_image = "";
                if($arg[$block_name . 'image']){
                    $bg_image = $arg[$block_name . 'image']['url'];
                }
            ?>

            <?php if( isset( $video ) || isset( $arg[$block_name . 'image']  ) ) : ?>
                <div class="img-block-wrap video" id="<?php echo $slide_id; ?>" 
                    <?php if(!$video && $bg_image) : ?>
                            style="background-image: url(<?php echo $bg_image; ?>);"
                    <?php endif; ?>
                >
                    <?php if($video && "youtube" != $video_type) :  ?>

                        <div class="gallery-video" style="position: relative;  top: 0px; left: 0px; bottom: 0px; right: 0px; overflow: hidden; background-size: cover; background-color: transparent; background-repeat: no-repeat; background-position: 50% 50%; background-image: none;">

                            <video poster="<?php echo $bg_image; ?>" loop="" muted="muted"   style="margin: auto; position: absolute; z-index: -1; top: 50%; left: 50%; transform: translate(-50%, -50%); visibility: visible; opacity: 1; object-fit: cover;"><source src="<?php echo $video; ?>" type="<?php echo $video_type;?>"></video>
                        </div><!--/gallery-video-->
                    <?php endif; ?>
                    <?php if($video && "youtube" == $video_type) :  ?>
                        <div class="gallery-video" style="position: relative;  top: 0px; left: 0px; bottom: 0px; right: 0px; overflow: hidden; background-size: cover; background-color: transparent; background-repeat: no-repeat; background-position: 50% 50%; background-image: none;">
                            <iframe src="<?php echo $video; ?>" width="100%" height="100%" frameborder="0" allowfullscreen></iframe>
                        </div>
                    <?php endif; ?>

                            <div class="gallery-container" >
                                <?php if($video && "youtube" != $video_type) : ?>
                                    <a class="video-button-inline ">
                                        <svg class="play" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                        viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
                                            <path d="M20,0C9,0,0,9,0,20s9,20,20,20c11,0,20-9,20-20S31,0,20,0z M15.4,30V10l13.3,10L15.4,30z" />
                                        </svg>
                                    </a>
                                <?php endif; ?>


                            </div><!--/gallery-container-->

                    </div><!--/img-block-wrap-->
                
            <?php endif; ?>
        <?php endforeach; ?>
    </div><!--/slick-slider-->
    <div class="nav-wrapper-wrapper">
        <div class="nav-wrapper"></div>
    </div>

    <div class="subtitle-wrapper">
        <div id="slick-caption-<?php echo $block_id; ?>">
            <?php foreach ( $args['slides'] as $arg ) : ?>
                <div class="subtitle">
                    <?php if( isset( $arg[$block_name . 'subtitle'] ) && $arg[$block_name . 'subtitle'] ) : ?>                            
                        <p><?php echo $arg[$block_name . 'subtitle']; ?></p>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
    <script>
        jQuery(document).ready(function($){
            $('#slick-slider-<?php echo $block_id; ?> .video-btn').magnificPopup({
          type: 'iframe'
      });
        <?php if(count($args['slides'])>0) : ?>
          $('#slick-slider-<?php echo $block_id; ?>').slick({
            slidesToShow:1,
            dots:false,
            arrows:true,
            asNavFor: '#slick-caption-<?php echo $block_id; ?>',
            appendArrows: $('#<?php echo $args['id']; ?> .nav-wrapper')            
            //adaptiveHeight: true
          });
          $('#slick-caption-<?php echo $block_id; ?>').slick({
            slidesToShow:1,
            asNavFor: '#slick-slider-<?php echo $block_id; ?>',
            arrows: false,

          });
$('.video-button-inline').click(function (e) {
    e.preventDefault();
    var $parent = $(e.target).closest('.img-block-wrap');
    var mediaVideo = $parent.find('video').get(0);
   if (mediaVideo.paused) {
       mediaVideo.play();
       $parent.find('.video-button-inline').addClass('playing');
   } else {
       mediaVideo.pause();
       $parent.find('.video-button-inline').removeClass('playing');
  }
});
        <?php endif; ?>

        });

      
    </script>
<?php endif; ?>
