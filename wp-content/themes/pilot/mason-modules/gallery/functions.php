<?php
	function build_gallery_layout( $params ){
		global $i;
		$incr = 1;

		$slides = mason_get_sub_field('gallery_block_slides');
		$args = [
			'slides' => []
		];
		$skip_forced = 0;
		$args['skip_header'] = 0;
		$args['module_classes']['height_class'] = mason_get_sub_field('gallery_block_height') ? 'gallery-tall' : 'gallery-short';
		if(is_array($slides) && count($slides)>0){
			foreach($slides as $slide){
				$slide['gallery_block_slide_id'] = 'gallery_block_slide_'.$i.'_#'.$incr;
				$incr++;

				$args['slides'][] = $slide;
			}
			$skip_forced = 1;
		}
		if(!$skip_forced && ($params['forced'] || is_single())){ // should check what cpt
			$type = get_post_type();
			if(get_field($type.'_disable_header') && $params['forced'] ){
				$args['skip_header'] = 1;
			}
			else{
				$img = get_field($type . '_image');
				$icon_obj = get_field($type . '_icon');
				$url = "";
				$title = "";
				if('person' != $type){
					$title = get_the_title();
				}
			    $args['slides'][] = [
			        'gallery_block_use_popup' => "",
			        'gallery_block_image' => $img,
			        'gallery_block_logo_or_title' => "title",
			        'gallery_block_title' => $title,
			        'gallery_block_slide_id' => "gallery_block_slide_0_#1",
			        'gallery_block_bg_video_file_mp4' => "",
			        'gallery_block_overlay_color' => "",
			        'gallery_block_overlay_opacity' => "",
			        'gallery_block_icon' => $icon_obj

			    ];
			}
		}
		$args['acf_incr'] = "_forced";
		$args['module_styles'] = [];
		if(get_sub_field('gallery_block_margin-top')){
			$args['module_styles']['margin-top'] = get_sub_field('gallery_block_margin-top');
		}
		if(get_sub_field('gallery_block_margin-bottom')){
			$args['module_styles']['margin-bottom'] = get_sub_field('gallery_block_margin-bottom');
		}


//		print_r($args);
		return $args;
/*
		$custom_classes = " " . mason_get_sub_field('gallery_block_custom_class');
		$button_object = ( mason_get_sub_field('gallery_block_button_href') ? mason_get_sub_field('gallery_block_button_href') : mason_get_sub_field('gallery_block_custom_button_href') );
		if( is_object( $button_object ) ){
			$button_href = get_permalink( $button_object->ID);
		}
		else{
			$button_href = $button_object;
		}
		$gallery_args = array(
			'subtitle' => mason_get_sub_field('gallery_block_subtitle'),
			'button_text' => mason_get_sub_field('gallery_block_button_text'),
			'button_href' => $button_href,
			'id' => 'gallery_block_'.$i,
			'overlay_color' => '',
			'overlay_opacity' => '',
		);
		$gallery_args['title'] = mason_get_sub_field('gallery_block_title');
		$gallery_args['logo'] = mason_get_sub_field('gallery_block_logo');
		if( mason_get_sub_field('gallery_block_modify') ){
			if( $opacity = mason_get_sub_field('gallery_block_overlay_opacity') ){
				$gallery_args['overlay_opacity'] = $opacity;
			}
			if( $color = mason_get_sub_field('gallery_block_overlay_color') ){
				$gallery_args['overlay_color'] = $color;
			}
		}				
		$image = mason_get_sub_field('gallery_block_image');
		if( is_array( $image ) ){
			$gallery_args['image_url'] = $image['url'];
		}
		$mp4_file = mason_get_sub_field('gallery_video_file_mp4');
		if( is_array( $mp4_file ) ){
			$gallery_args['width_class'] = $gallery_args['width_class'] . " video-gallery";
			$gallery_args['video_file_mp4'] = $mp4_file['url'];
			$gallery_args['fallback_image_url'] = $image['url'];
		}
		$args[] = $gallery_args;
		*/

	}

?>