<?php
	function build_quote_layout(){
		global $i;
		$args = [];
		$quotes = [];
		$rows_arr = mason_get_sub_field('quote_block_rows');
		$image = mason_get_sub_field('quote_block_image');
		$args['overlay_color'] = mason_get_sub_field('quote_block_overlay_color');
		$args['overlay_opacity'] = mason_get_sub_field('quote_block_overlay_opacity');
		if( is_array( $image ) ){
			$args['bg_image_url'] = $image['url'];
		}
		if( is_array($rows_arr) ){
			$rows = array();
			foreach( $rows_arr as $row_arr){
				$quote_args = array(
					'title' => $row_arr['quote_block_quote'],
					'img' => $row_arr['quote_block_slide_image'],
					'id' => 'quote_block'.'_'.$i,
					'caption' => $row_arr['quote_block_content'],
				);
				$quotes[] = $quote_args;
			}
		}
		$args['quotes'] = $quotes;
		$args['module_styles'] = [];
		if(get_sub_field('quote_block_margin-top')){
			$args['module_styles']['margin-top'] = get_sub_field('quote_block_margin-top');
		}
		if(get_sub_field('quote_block_margin-bottom')){
			$args['module_styles']['margin-bottom'] = get_sub_field('quote_block_margin-bottom');
		}
		
		if( is_array( $args ) ){
			return $args;
		}
	}
	function quote_admin_enqueue($hook) {
		wp_register_style( 'quote_wp_admin_css', get_template_directory_uri() . '/mason-modules/quote/admin-style.css', false, '1.0.0' );
        wp_enqueue_style( 'quote_wp_admin_css' );
    }
	add_action( 'admin_enqueue_scripts', 'quote_admin_enqueue' );
?>