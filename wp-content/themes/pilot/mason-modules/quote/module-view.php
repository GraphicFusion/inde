<?php 
	global $args; 
?>
<div class=" container-fluid container-lg container-md container-sm ">
	<div class="card"  style="background-image: url(' <?php echo $args['bg_image_url']; ?>');">
		<div class="img-overlay"> 
			<div class="filter" 
			style="background-image:linear-gradient(to right, <?php echo $args['overlay_color']; ?> 100%, transparent ); opacity: <?php echo $args['overlay_opacity']; ?>;"></div>
		</div>
		<div class="row">
			<div class="col-lg-12" >
				<div class="container-fluid  inner-container">
					<div class="shadow-quote">

<?php
	if(count($args['quotes']) > 0 ) :
		$i = 0;
		foreach($args['quotes'] as $quote):  ?>
				<div class="quote-foot-content">
						<div class="title-wrap">
							<?php if( $quote['img']  ) : ?>
								<img src="<?php echo $quote['img']['url']; ?>" class="img">
							<?php endif; ?>
							<?php if( $quote['title']  ) : ?>
								<h5><?php echo $quote['title']; ?></h5>
							<?php endif; ?>
							<?php if(  $quote['caption']  ) : ?>
								<div class="content"><?php echo $quote['caption']; ?></div>
							<?php endif; ?>
						</div><!--/title-wrap-->
				</div><!--/quote-foot-content-->
		<?php endforeach; ?>
	<?php endif; ?>
					</div>
						<div class="nav-wrapper-wrapper">
							<div class="nav-wrapper"></div>
						</div>
				</div><!--/shadow-quote-->
			</div>
		</div>
	</div>
</div>
<script>
	$( document ).ready(function() {
		$('#<?php echo $args['id']; ?> .shadow-quote').slick({
			slidesToShow:1,
			dots:false,
			arrows:true,
			appendArrows: $('#<?php echo $args['id']; ?> .nav-wrapper'),
		});


	});
</script>