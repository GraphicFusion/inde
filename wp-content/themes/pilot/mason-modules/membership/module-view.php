<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 * array	$args['rows']			 //  array of rows of lede/content
	 * string	$args['rows'][0]['lede'] //  row title
	 * string	$args['rows'][0]['hidden_content'] //  row hidden content
	 */
	global $args; 
	$suffixes = [
		'_one',
		'_two',
		'_three'
	];
?>
<div class="container-fluid  container-card  container-sm container-md container-lg">
	<div class="row">
		<?php foreach($suffixes as $suffix)  : ?>
			<div class="col-xs-12 col-lg-4 mem-col">
				<div class="membership-wrapper">
					<div class="membership">
						<div class="title-wrapper">
							<div class="title">
								<h5><?php echo $args['title'.$suffix]; ?></h5>
							</div>
							<div class="tog-wrapper">
								<div class="acc-tog">
									<div class="horizontal"></div>
									<div class="vertical"></div>
								</div>
							</div>
						</div>
						<div class="content">
							<?php if( is_array( $args['rows'.$suffix] ) ) : ?>
								<?php foreach( $args['rows'.$suffix] as $row ): ?>
									<div class="mem-row">
										<div class="label">
											<?php echo $row['membership_block_label'.$suffix]; ?>
										</div>
										<div class="value">
											<div class="text">	
												<?php if($row['membership_block_toggle'.$suffix]) : ?>
													<?php echo $row['membership_block_value'.$suffix]; ?>
												<?php else : ?>
													<div class="check"></div>
												<?php endif; ?>
											</div>
										</div>
									</div>
								<?php endforeach; ?>
								<div class="mem-row btn-wrapper">
									<a class="inde-btn" href="<?php echo $args['button']['url']; ?>" target="<?php echo $args['button']['target']; ?>">
										<?php echo $args['button']['title']; ?>
									</a> 
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
	<?php if($args['caption']) : ?>
	<div class="row">
		<div class="col-lg-12 mem-col">
			<div class="caption"><p class="small"><?php echo $args['caption']; ?></p></div>
		</div>
	</div>
	<?php endif;?>
</div>
<script>
	jQuery(document).ready(function($) {
	// Accordion script
		var allMembPanels = $('.membership > .content').hide(),
			allMembPanelLabels = $('.membership > .title-wrapper');
		function resizeAccordion(){
			var winWidth = $(window).width();
			if(1259 < winWidth){
console.log('greater',$('.membership .content')[0]);
				$('.membership .content').css('display','inline-block').show();
				$('.title-wrapper').addClass('active js-open');
			}
			else{
console.log('less', winWidth);
				$('.membership .content').hide();
				$('.title-wrapper').removeClass('active js-open');
			}			
		}
		resizeAccordion();
		$(window).on('resize', function(){
			//resizeAccordion();
		});
	
			
		$('.membership  .acc-tog').click(function() {
			if ( !$(this).closest('.title-wrapper').hasClass('active js-open') ) {
				allMembPanels.slideUp();
				allMembPanelLabels.removeClass('active js-open');
				$(this).closest('.title-wrapper').next().slideDown();
				$(this).closest('.title-wrapper').addClass('active js-open');
			}
			else {
				$(this).closest('.title-wrapper').removeClass('active js-open').next().slideUp();
			}
			return false;
		});
	});

</script>