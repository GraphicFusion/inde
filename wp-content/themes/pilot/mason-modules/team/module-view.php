<?php
	global $args;
?>
<?php if(is_array($args['rows'])): $row_counter = 0; ?>
	<div class='team-section'>
		<div class='team-content-section interior-box-wide container-fluid container-sm container-md container-lg'>
			<div class="row">
			<script>
				var team = {};
			</script>
				<?php $i = 1; foreach( $args['rows'] as $row ) : ?>
					<script>
						var member<?php echo $i; ?> = {};
						member<?php echo $i; ?>.name = '<?php echo $row['name']; ?>';
						member<?php echo $i; ?>.text = '<?php echo preg_replace("#'#","\'",$row['text']); ?>';
						member<?php echo $i; ?>.title = '<?php echo $row['title']; ?>'; 
						team[<?php echo $i; ?>] = member<?php echo $i; ?>;
					</script>
					<div class='member shadow-card col-xs-12 col-md-6 col-lg-4' data-member='<?php echo $i; ?>'>
							<div class=" member-shadow">
								<div class='team-block-image '>
									<img src='<?php echo $row['image']['url']; ?>'>
								</div>
								<div class="member-content ">
									<div class="content-wrapper">
										<h5 class="member-name"><?php echo $row['name']; ?></h5>
										<div class="member-title"><?php echo $row['title']; ?></div>
									</div>
								</div>
						</div>
					</div><!--team-content-section--->
				<?php $i++; endforeach; ?>
			</div>
  		</div><!--/team-section--->
	</div>
	<script>
		function wrapTeam(){
			$('.popup-content').unwrap();
			$('.popup-content').remove();
			
		 	var divs = $(".member"),
		 	width = $(window).width();
		 	$incr = 3;
		 	if(width <= 1259 ){
		 		$incr = 2;
		 	}
		 	if(width <= 959 ){
		 		$incr = 1;
		 	}
		 	for(var i = 0; i < divs.length; i+=$incr) {
  				divs.slice(i, i+$incr).wrapAll("<div class='member-row' style='width:100%''></div>");
			}
			$('.member-row ').append('<div class="popup-content shadow-card"><div class="popup-wrapper container-md container-lg container-fluid container-card"><div class="content-wrapper"><h5 class="name"></h5><p class="title"></p><p class="text"></p></div></div></div>');
			$.each( $('.member.shadow-card'), function(c,v){
				revealBio(v);
			})
			$('.member.shadow-card').removeClass('active');
		 	$('.popup-content').removeClass('active');
		}
		function revealBio($this){
				if( $($this).hasClass('active') ){
					$('.member.shadow-card').removeClass('active');
				 	$('.popup-content').removeClass('active');		
				 	//alert(1);			
				}
				else{
			 		$('.member.shadow-card').removeClass('active');
			 		var member_id = $($this).data('member'),
			 		member = team[member_id];
			 		$('.popup-content').removeClass('active');
			 		var $popup = $($this).siblings('.popup-content');
			 		$($this).addClass('active');
			 		$popup.find('.name').html(member.name);
			 		$popup.find('.title').html(member.title);
			 		$popup.find('.text').html(member.text);
			 		$popup.addClass('active');
			 	}
		}
		$(document).ready(function() {
			$(window).on('resize', function(){
				wrapTeam();
			});
		 	wrapTeam();
		 	$('.member.shadow-card').on('click',function(){
		 		revealBio( this );
		 	});
		 });
	</script>
<?php endif; ?>