<?php
	function build_navigation_layout(){
		$args = array(
			'logo' => mason_get_sub_field('navigation_block_logo'),
			'secondary_logo' => mason_get_sub_field('navigation_block_logo_secondary'),
			'main_links' => mason_get_sub_field('navigation_block_main_nav'),
			'social' => mason_get_sub_field('navigation_block_social_links'),
			'extra' => mason_get_sub_field('navigation_block_extra_links'),
			'directions' => mason_get_sub_field('navigation_block_directions')
		);

	    $weather = get_transient( 'inde_weather' );
	    if(!$weather){
			$url = "http://api.openweathermap.org/data/2.5/weather?id=5321088&appid=7edbeb7fbda7706523fd61e9194e368e";
		    $rCurlHandle    = curl_init($url);
		    $type = "GET";
		    curl_setopt($rCurlHandle, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($rCurlHandle, CURLOPT_FOLLOWLOCATION, true);
		    curl_setopt($rCurlHandle, CURLOPT_MAXREDIRS, 3);
		    curl_setopt($rCurlHandle, CURLOPT_SSL_VERIFYPEER, true);
		    curl_setopt($rCurlHandle, CURLOPT_SSL_VERIFYHOST, 2);
		    curl_setopt($rCurlHandle, CURLOPT_CONNECTTIMEOUT, 30);
		    curl_setopt($rCurlHandle, CURLOPT_TIMEOUT, 30);
		    curl_setopt($rCurlHandle, CURLOPT_CUSTOMREQUEST, $type);
		    curl_setopt($rCurlHandle, CURLOPT_HTTPHEADER, ['Content-Type: application/json; charset=utf-8', 'Expect:']);
		    curl_setopt($rCurlHandle, CURLOPT_POSTFIELDS, json_encode($variant));
		    $response = curl_exec ($rCurlHandle );
		    curl_close ($rCurlHandle );
		    $output = json_decode($response);
			$weather = $output->weather[0]->description; 
			$temp = round(($output->main->temp - 273.15) * (9/5) + 32);
			set_transient( 'inde_weather', $weather , 60*60*2 );
			set_transient( 'inde_temp', $temp, 60*60*2 );
		}
		else{
			$temp = get_transient( 'inde_temp' );
		}
		$args['weather'] = $temp."&#176;F / ".$weather;
		return $args;
	}
?>