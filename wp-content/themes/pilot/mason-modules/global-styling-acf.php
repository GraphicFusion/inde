<?php
	function get_styling_fields( $module ){
		$included_modules = [
			'media',
			'slideshow'
		];
		if(1 == 1){
			$module = $module . "_block_";
			$fields =  array(
				/*
				array(
					'key' => create_key($module,'module_styling'),
					'label' => 'Styling Parameters',
					'name' => $module.'_module_styling',
					'type' => 'true_false',
					'type' => 'true_false',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'default_value' => 0,
					'ui' => 1,
					'ui_on_text' => 'Hide Styles',
					'ui_off_text' => 'Show Styles',
				),
				*/
				array(
					'key' => create_key($module,'top_margin'),
					'label' => 'Top Margin',
					'name' => $module.'margin-top',
					'type' => 'text',
					'instructions' => 'Add margin to Module as: "10%" or "10px"',
					'required' => 0,
					'conditional_logic' => array(
						/*
						array(
							array(
								'field' => create_key($module,'module_styling'),
								'operator' => '==',
								'value' => '1',
							),
						),
						*/
					),
					'wrapper' => array(
						'width' => '30%',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array(
					'key' => create_key($module,'bottom_margin'),
					'label' => 'Bottom Margin',
					'name' => $module.'margin-bottom',
					'type' => 'text',
					'instructions' => 'Add margin to Module as: "10%" or "10px"',
					'required' => 0,
					'conditional_logic' => array(
						/*
						array(
							array(
								'field' => create_key($module,'module_styling'),
								'operator' => '==',
								'value' => '1',
							),
						),
						*/
					),
					'wrapper' => array(
						'width' => '30%',
						'class' => '',
						'id' => '',
					),
					'default_value' => '100px',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array(
					'key' => create_key($module,'z_index'),
					'label' => 'Stacking Layer (z-index)',
					'name' => $module.'z-index',
					'type' => 'text',
					'instructions' => 'Modify z-index of this module and the one below to alter the shadow effect (0-9999999). This is only applied to Generic, Features, Accordion, and the Half & Half Modules.',
					'required' => 0,
					'conditional_logic' => array(
						/*
						array(
							array(
								'field' => create_key($module,'module_styling'),
								'operator' => '==',
								'value' => '1',
							),
						),
						*/
					),
					'wrapper' => array(
						'width' => '30%',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
			);
			return $fields;
		}
	}
?>