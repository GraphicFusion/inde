<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */
	global $args;
?>
<div class="container-fluid container-md container-lg container-sm">
	<div class="insta-wrap ">
		<h6 class="insta-caption"><?php echo $args['text']; ?></h6>
		<div class="insta-content">
			<?php //echo do_shortcode('[instagram-feed num=4 cols=4 showfollow=false]');
			$code = $args['code'];
			echo do_shortcode($code);
			 ?>
		</div>
	</div>
</div>