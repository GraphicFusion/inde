<?php
	global $pilot;
	// add module layout to flexible content 
	$module_layout = array (
		'key' => create_key('insta','block'),
		'name' => 'insta_block',
		'label' => 'Instagram Feed',
		'display' => 'block',
		'sub_fields' => array (
			array (
		        'key' => create_key('insta','text'),
				'label' => 'Header',
				'name' => 'insta_block_text',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 1,
			),
			array (
		        'key' => create_key('insta','shortcode'),
				'label' => 'Shortcode',
				'name' => 'insta_block_shortcode',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 1,
			),
			array (
		        'key' => create_key('insta','content'),
				'label' => '',
				'name' => 'insta_block_message',
				'type' => 'message',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 1,
			),
		),
		'min' => '',
		'max' => '',
	);
?>