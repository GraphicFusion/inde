<?php
	function build_insta_layout(){
		$args = array(
			'text' => mason_get_sub_field('insta_block_text'),
			'code' => mason_get_sub_field('insta_block_shortcode')
		);
		$args['module_styles'] = [];
		if(get_sub_field('insta_block_margin-top')){
			$args['module_styles']['margin-top'] = get_sub_field('insta_block_margin-top');
		}
		if(get_sub_field('insta_block_margin-bottom')){
			$args['module_styles']['margin-bottom'] = get_sub_field('insta_block_margin-bottom');
		}

		return $args;
	}
?>