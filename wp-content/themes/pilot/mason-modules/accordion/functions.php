<?php
	function build_accordion_layout(){
		$args = array(
			'title' => mason_get_sub_field('accordion_block_title'),
			'content' => mason_get_sub_field('accordion_block_content'),
			'rows' => mason_get_sub_field('accordion_block_rows')
		);
		$args['module_styles'] = [];
		if(get_sub_field('accordion_block_margin-top')){
			$args['module_styles']['margin-top'] = get_sub_field('accordion_block_margin-top');
		}
		if(get_sub_field('accordion_block_z-index')){
			$args['module_styles']['z-index'] = get_sub_field('accordion_block_z-index');
		}
		if(get_sub_field('accordion_block_margin-bottom')){
			$args['module_styles']['margin-bottom'] = get_sub_field('accordion_block_margin-bottom');
		}


		return $args;
	}
?>