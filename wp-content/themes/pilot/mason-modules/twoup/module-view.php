<?php global $args; ?>
<div class="container-twoup container-fluid container-lg container-md container-sm">
<?php
	if(count($args['twoups']) > 0 ) :
		$i = 0;
		foreach($args['twoups'] as $twoup): ?>
			<?php if(0 == $i%2 ) : ?>
				<div class="row">
			<?php endif; ?>
			<div class="shadow-twoup col-12 col-lg-6" >
				<div class="shadow-card">
						<?php if($twoup['bg_image_url']) : ?>
					<div class="img-block-wrap" id="<?php echo $args['id']; ?>">
							<div class="bg-image" style="background-image: url(' <?php echo $twoup['bg_image_url']; ?>');">
							</div><!--/bg-img-->
					</div><!--img-block-wrap-->
						<?php endif; ?>
					<div class="twoup-foot-content">
							<div class="title-wrap">
								<?php if( $twoup['title']  ) : ?>
									<h6><?php echo $twoup['title']; ?></h6>
								<?php endif; ?>
								<?php if(  $twoup['content']  ) : ?>
									<div class="content"><?php echo $twoup['content']; ?></div>
								<?php endif; ?>
								<?php if( $twoup['button']  ) : ?>
										<a class="inde-btn" href="<?php echo $twoup['button']['url']; ?>" target="<?php echo $twoup['button']['target']; ?>"><?php echo $twoup['button']['title']; ?>
										</a>
								<?php endif; ?>
							</div><!--/title-wrap-->
					</div><!--/twoup-foot-content-->
				</div>
			</div><!--/shadow-twoup-->
			<?php 
			if($i%2 || count($args['twoups']) == ($i + 1) ) : ?>
				</div><!--/row-->
			<?php endif; ?>
		<?php $i++; endforeach; ?>
	<?php endif; ?>
</div>