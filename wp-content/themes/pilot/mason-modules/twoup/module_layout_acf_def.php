<?php
global $pilot;
// add module layout to flexible content


    // add module layout to flexible content 
    $name = "twoup";
    $module_layout = array (
        'key' => create_key($name, 'block'),
        'name' => 'twoup_block',
        'label' => 'Two Up Callout',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => create_key($name,'rows'),
                'label' => 'Callouts',
                'name' => 'twoup_block_rows',
                'type' => 'repeater',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'collapsed' => '',
                'min' => '2',
                'max' => '',
                'layout' => 'block',
                'button_label' => 'Add a TwoUp Card',
                'sub_fields' => array (

                    array(
                        'key' => create_key('twoup','image'),
                        'label' => 'Background Image',
                        'name' => 'twoup_block_image',
                        'type' => 'image',
                        'instructions' => '',
                        'required' => 0,
                        'wrapper' => array(
                            'width' => '30%',
                            'class' => "",
                            'id' => '',
                        ),
                        'return_format' => 'array',
                        'preview_size' => 'thumbnail',
                        'library' => 'all',
                        'min_width' => '',
                        'min_height' => '',
                        'min_size' => '',
                        'max_width' => '',
                        'max_height' => '',
                        'max_size' => '',
                        'mime_types' => '',
                    ),
                    array (
                        'key' => create_key($name,'title'),
                        'label' => 'Title',
                        'name' => 'twoup_block_title',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => "",
                            'id' => '',
                        ),
                        'default_value' => '',
                        'tabs' => 'all',
                        'toolbar' => 'full',
                        'twoup_upload' => 1,
                    ),
                    array (
                        'key' => create_key('twoup','content'),
                        'label' => 'Content' ,
                        'name' => 'twoup_block_content',
                        'type' => 'wysiwyg',
                        'instructions' => '',
                        'required' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => "",
                            'id' => '',
                        ),
                        'default_value' => '',
                        'tabs' => 'all',
                        'toolbar' => 'full',
                        'twoup_upload' => 1,
                    ),
                    array(
                        'key' => create_key('twoup','link'),
                        'label' => 'Button Link',
                        'name' => 'twoup_block_link',
                        'type' => 'link',
                        'instructions' => '',
                        'required' => 0,
                        'wrapper' => array(
                            'width' => '100',
                            'class' => "",
                            'id' => '',
                        ),
                        'post_type' => '',
                        'taxonomy' => '',
                        'allow_null' => 0,
                        'multiple' => 0,
                        'return_format' => 'object',
                        'ui' => 1,
                    ),
                ),

            ),
        ),
    );
?>