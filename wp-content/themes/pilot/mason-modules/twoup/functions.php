<?php
	function build_twoup_layout(){
		global $i;
		$args = [];
		$twoups = [];
		$rows_arr = mason_get_sub_field('twoup_block_rows');
		if( is_array($rows_arr) ){
			$rows = array();
			foreach( $rows_arr as $row_arr){
				$image = $row_arr['twoup_block_image'];
				$twoup_args = array(
					'subtitle' => $row_arr['twoup_block_subtitle'],
					'title' => $row_arr['twoup_block_title'],
					'button' => $row_arr['twoup_block_link'],
					'id' => 'twoup_block'.'_'.$i,
					'content' => $row_arr['twoup_block_content'],
				);
				$twoup_args['bg_image_url'] = "";
				if( is_array( $image ) ){
					$twoup_args['bg_image_url'] = $image['url'];
				}
				$twoups[] = $twoup_args;
			}
		}
		$args['twoups'] = $twoups;
		$args['module_styles'] = [];
//		print_r($args);
		if(get_sub_field('twoup_block_margin-top')){
			$args['module_styles']['margin-top'] = get_sub_field('twoup_block_margin-top');
		}
		if(get_sub_field('twoup_block_margin-bottom')){
			$args['module_styles']['margin-bottom'] = get_sub_field('twoup_block_margin-bottom');
		}
		
		if( is_array( $args ) ){
			return $args;
		}
	}
?>