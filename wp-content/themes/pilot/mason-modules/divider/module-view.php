<?php
	/**
	 * string	$args['rows']
	 * string	$args['rows'][0]['image']
	 * string	$args['rows'][0]['title']
	 * string	$args['rows'][0]['text']
	 * string	$args['rows'][0]['button_text']
	 * string	$args['rows'][0]['button_link']
	 */
	global $args;
?>
<div class="container-fluid container-lg container-md container-sm">
	<div class="row">
		<div class="col-xs-2">
		</div>
		<div class="col-xs-8 img-wrapper">
			<img class='alt-block-image' src="<?php echo $args['image']['url']; ?>">
		</div>
	</div>
</div>