<?php
	global $pilot;
	$name = "divider";
	// add module layout to flexible content 
	$module_layout = array (
		'key' => create_key($name,'block'),
		'name' => 'divider_block',
		'label' => 'Divider Float',
		'display' => 'block',
		'sub_fields' => array (
				array (
					'key' => create_key($name,'image'),
					'label' => 'Image',
					'name' => 'divider_block_image',
					'type' => 'image',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_format' => 'array',
					'preview_size' => 'thumbnail',
					'library' => 'all',
					'min_width' => '',
					'min_height' => '',
					'min_size' => '',
					'max_width' => '',
					'max_height' => '',
					'max_size' => '',
					'mime_types' => '',
				),
		),
		'min' => '',
		'max' => '',
	);
?>