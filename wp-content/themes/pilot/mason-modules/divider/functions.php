<?php
	function build_divider_layout(){
		$args = [];
		$args['image'] = mason_get_sub_field('divider_block_image');
		$args['module_styles'] = [];
		if(get_sub_field('divider_block_margin-top')){
			$args['module_styles']['margin-top'] = get_sub_field('divider_block_margin-top');
		}
		if(get_sub_field('divider_block_margin-bottom')){
			$args['module_styles']['margin-bottom'] = get_sub_field('divider_block_margin-bottom');
		}
		
		return $args;
	}

?>