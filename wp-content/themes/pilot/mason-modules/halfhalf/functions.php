<?php
	function build_halfhalf_layout(){
		$rows_arr = mason_get_sub_field('halfhalf_block_rows');
		$args = [];
		$args['position'] = mason_get_sub_field('halfhalf_block_left');
		if( is_array($rows_arr) ):
			$rows = array();
			foreach( $rows_arr as $row_arr){
				$rows[] = array(
					'title' => $row_arr['halfhalf_block_title'],
					'button' => $row_arr['halfhalf_block_button'],
					'image' => $row_arr['halfhalf_block_image'],
				);
				
			}
			while ( mason_have_rows('halfhalf_block_rows') ) : the_row();
				$rows[] = array(
					'title' => get_sub_field('halfhalf_block_title'),
					'button' => get_sub_field('halfhalf_block_button'),
					'image' => get_sub_field('halfhalf_block_image'),
				);
			endwhile;
			$args['rows'] = $rows;
		endif;
		$args['title'] = mason_get_sub_field('halfhalf_block_title');
		$args['button'] = mason_get_sub_field('halfhalf_block_button');

		$args['module_styles'] = [];
		if(get_sub_field('halfhalf_block_margin-top')){
			$args['module_styles']['margin-top'] = get_sub_field('halfhalf_block_margin-top');
		}
		if(get_sub_field('halfhalf_block_z-index')){
			$args['module_styles']['z-index'] = get_sub_field('halfhalf_block_z-index');
		}
		if(get_sub_field('halfhalf_block_margin-bottom')){
			$args['module_styles']['margin-bottom'] = get_sub_field('halfhalf_block_margin-bottom');
		}

		return $args;
	}
function wp_durazo_enqueue_scripts() {
	wp_enqueue_script( 'slide_script', get_template_directory_uri() . "/mason-modules/halfhalf/script.js", array('jquery'), null, true );
	//wp_enqueue_script( 'slick_script', get_template_directory_uri() . "/mason-modules/halfhalf/slick.min.js" , array('jquery'), null, true);
	wp_enqueue_style( 'slick_style',"https://kenwheeler.github.io/slick/slick/slick-theme.css");
	wp_enqueue_style( 'slick_theme_style',"https://kenwheeler.github.io/slick/slick/slick.css");

	wp_enqueue_script( 'popup_script', get_template_directory_uri() . "/mason-modules/halfhalf/html5lightbox.js" , array('jquery'), null, true);
	//wp_enqueue_style( 'popup_style',get_template_directory_uri() . "/mason-modules/slides/slick-lightbox.css");

}
add_action( 'wp_enqueue_scripts', 'wp_durazo_enqueue_scripts' );
add_action( 'wp_enqueue_scripts', 'wp_durazo_enqueue_scripts' );

?>