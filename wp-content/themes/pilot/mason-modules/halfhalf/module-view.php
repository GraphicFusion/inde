<?php
	global $args;
	global $wp;
	$current_url = home_url( add_query_arg( array(), $wp->request ) );
	$block_id = $args['acf_incr'];
?>
<?php if( isset($args['parent_title'])):?>
	<div class="parent-header">
		<div class="container content-container interior-container" >
			<div class="title">
				<?php echo $args['title']; ?>
			</div><!--/title-->
		</div><!--/container--> 			
	</div>
	<script>
		$( document ).ready(function() {
			$('body').addClass('has-parent-header');
		});
	</script>
<?php else: ?>
	<?php if(is_array($args['rows'])): $row_counter = 0; ?>
	<div class='slides-section'>
		<div class="feature-wrapper ">
			<div class='half-half slick-slider image-<?php echo $args['position']; ?>' id="slick-slider-<?php echo $block_id; ?>">
				<?php foreach( $args['rows'] as $row ) : ?>
					<img class='slides-block-image' src='<?php echo $row['image']['url']; ?>'>
				<?php endforeach; ?>
	  		</div>

	  		<div class="caption-wrapper image-<?php echo $args['position']; ?> ">
				<div class="slick-caption half-half">
					<div class="caption" >
						<div class="caption-inner-wrapper" >
							<?php if(isset($args['title']) ) : ?>
								<div class="content"><?php echo $args['title']; ?></div>
							<?php endif; ?>
							<?php if(isset($args['button']) && is_array($args['button']) && array_key_exists('url',$args['button'])) : ?>
								<a class="inde-btn" href='<?php echo $args['button']['url']; ?>'><?php echo $args['button']['title']; ?></a>
							<?php endif; ?>
						</div>
					</div>
				</div>
	  			<?php if(count($args['rows'])>1 ): ?>
	  				<div class="nav-wrapper-wrapper dd">
	  				<div class="nav-wrapper-inner" style="display:inline-block;">
					<div class="nav-wrapper" id="nav-wrapper-<?php echo $block_id; ?>">
					</div>
				</div>
					</div>
				<?php endif; ?>
		  	</div><!--/caption-wrpper-->
		  	
		</div><!--/feature-wrapper-->
	</div><!--/slides-section-->
	<script>
		<?php if(count($args['rows']>1)) : ?>
		$(document).ready(function(){
			var $width = 959;
			var adaptiveHeight = false;
			if($(window).width() < $width ){
				adaptiveHeight = true;

			} 
//		  var $appendArrows = $('#nav-wrapper-mobile-<?php echo $block_id; ?>');
				  var $appendArrows = $('#nav-wrapper-<?php echo $block_id; ?>');
				if($(window).width() > $width ){
				}
	  $('.half-half#slick-slider-<?php echo $block_id; ?>').on('init', function(event, slick, currentSlide, nextSlide){
				if($(window).width() > $width ){
					var height = $('#halfhalf_block_<?php echo $block_id; ?>').find('.caption-inner-wrapper').outerHeight();
					var addPaddingTop = $('#halfhalf_block_<?php echo $block_id; ?>').find('.caption').css('padding-top');			//
					height = height + parseInt(addPaddingTop);
					if(height < 640 ){
						height = 640;
					}
					$('.half-half#slick-slider-<?php echo $block_id; ?>').closest('.feature-wrapper').height(height);
					$('.half-half#slick-slider-<?php echo $block_id; ?>').find('.slick-list').height(height);
					$('.half-half#slick-slider-<?php echo $block_id; ?>').find('img').height(height);
					$('.half-half#slick-slider-<?php echo $block_id; ?>').height(height);
					$('.half-half#slick-slider-<?php echo $block_id; ?>').find('.caption').height(height);
				}
				else{
					$.each( $('.half-half#slick-slider-<?php echo $block_id; ?> img'), function(k,v){
						var width = $(v).width(),
						height = width * 2/3;
						$(v).height(height);
					})
				}
	  })
	  $('.half-half#slick-slider-<?php echo $block_id; ?>').slick({
		slidesToShow:1,
		dots:false,
		arrows:true,
		appendArrows:$appendArrows,
		adaptiveHeight: true,
	  });


});
	<?php endif; ?>
	</script>
<?php endif; ?>
<?php endif; ?>