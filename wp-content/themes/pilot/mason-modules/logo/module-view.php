<?php
    global $args;
    $block_name = 'logo_block_';   
    $block_id = $args['acf_incr'];
    unset($args['acf_incr']);
?>
<div id="slick-slider-<?php echo $block_id; ?>" style="">
    <?php foreach ( $args['slides'] as $arg ) : $slide_id = $arg[$block_name . "slide_id"]; 
            ?>
        <?php if( isset( $arg[$block_name . 'bg_video_file_mp4'] ) || isset( $arg[$block_name . 'image']  ) ) : ?>
            <div class="img-block-wrap video" id="<?php echo $slide_id; ?>" 
                <?php if(!$arg[$block_name . 'bg_video_file_mp4'] && $arg[$block_name . 'image']) : ?>
                        style="background-image: url(<?php echo $arg[$block_name . 'image']['url']; ?>);"
                <?php endif; ?>
            >
                <?php if($arg[$block_name . 'bg_video_file_mp4']) :  ?>
                    <div class="logo-video" style="position: absolute; z-index: -1; top: 0px; left: 0px; bottom: 0px; right: 0px; overflow: hidden; background-size: cover; background-color: transparent; background-repeat: no-repeat; background-position: 50% 50%; background-image: none;">
                        <video class="bg-vid" autoplay loop="" muted="muted"  style="margin: auto; position: absolute; z-index: -1; top: 50%; left: 50%; transform: translate(-50%, -50%); visibility: visible; opacity: 1;"><source src="<?php echo $arg[$block_name . 'bg_video_file_mp4']['url']; ?>" type="video/mp4"></video>
                    </div><!--/logo-video-->
                <?php endif; ?>
                    <div class="img-overlay"> 
                        <div class="filter" 
                        style="background-image:linear-gradient(to right, <?php echo $arg[$block_name . 'overlay_color']; ?> <?php echo ($arg[$block_name . 'overlay_percent'] ? $arg[$block_name . 'overlay_percent'] : 100); ?>%, transparent ); opacity: <?php echo $arg[$block_name . 'overlay_opacity']; ?>;"></div>

                        <div class="logo-container" >
                            <?php if( $arg[$block_name . 'logo'] ) : ?>
                                <img src="<?php echo $arg[$block_name . 'logo']['url']; ?>">
                            <?php endif; ?>
                        </div><!--/logo-container-->
                    </div><!--/img-overlay-->
                </div><!--/img-block-wrap-->
            </div>
        <?php endif; ?>
    <?php endforeach; ?>
</div><!--/slick-slider-->

    <script>
        jQuery(document).ready(function($){
        <?php if(count($args['slides'])>0) : ?>
            $('#slick-slider-<?php echo $block_id; ?>').slick({
                slidesToShow:1,
                dots:false,
                arrows:false,
            });
        <?php endif; ?>
        });      
    </script>
