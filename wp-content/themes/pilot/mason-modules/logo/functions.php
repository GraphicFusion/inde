<?php
	function build_logo_layout( $params ){
		global $i;
		$incr = 1;

		$slides = mason_get_sub_field('logo_block_slides');
		$args = [
			'slides' => []
		];
		$skip_forced = 0;
		if(is_array($slides) && count($slides)>0){
			foreach($slides as $slide){
				$slide['logo_block_slide_id'] = 'logo_block_slide_'.$i.'_#'.$incr;
				$incr++;

				$args['slides'][] = $slide;
			}
		}
		$args['acf_incr'] = "_forced";
		$args['module_styles'] = [];
		if(get_sub_field('logo_block_margin-top')){
			$args['module_styles']['margin-top'] = get_sub_field('logo_block_margin-top');
		}
		if(get_sub_field('logo_block_margin-bottom')){
			$args['module_styles']['margin-bottom'] = get_sub_field('logo_block_margin-bottom');
		}

		return $args;
	}

?>