<?php
global $pilot;
$name = 'logo';
// add module layout to flexible content
$module_layout = array(
    'key' => create_key($name,'block'),
    'name' => 'logo_block',
    'label' => 'Logo Block',
    'display' => 'block',
    'sub_fields' => array(
        array (
            'key' => create_key($name,'slides'),
            'label' => 'Hero Block',
            'name' => 'logo_block_slides',
            'type' => 'repeater',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'collapsed' => '',
            'min' => '1',
            'max' => '1',
            'layout' => 'block',
            'button_label' => 'Hero Image:',
            'sub_fields' => array (

            array(
                'key' => create_key($name,'image'),
                'label' => 'Image',
                'name' => 'logo_block_image',
                'type' => 'image',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                ),
                'wrapper' => array(
                    'width' => '30%',
                    'class' => '',
                    'id' => '',
                ),
                'return_format' => 'array',
                'preview_size' => 'thumbnail',
                'library' => 'all',
                'min_width' => '',
                'min_height' => '',
                'min_size' => '',
                'max_width' => '',
                'max_height' => '',
                'max_size' => '',
                'mime_types' => '',
            ),
            array(
                'key' => create_key($name,'bg_video_file_mp4'),
                'label' => 'Background Video MP4',
                'name' => 'logo_block_bg_video_file_mp4',
                'type' => 'file',
                'instructions' => 'Adding a video overrides an image. Choose a MP4 video with a resolution of at least 1280x720',
                'required' => 0,
                'conditional_logic' => array(
                ),
                'wrapper' => array(
                    'width' => '40%',
                    'class' => '',
                    'id' => '',
                ),
                'return_format' => 'array',
                'library' => 'all',
                'min_size' => '',
                'max_size' => '',
                'mime_types' => 'mp4',
            ),

            array(
                'key' => create_key($name,'modify'),
                'label' => 'Apply Filter Over Image/Video',
                'name' => 'logo_block_modify',
                'type' => 'true_false',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '30%',
                    'class' => '',
                    'id' => '',
                ),
                'message' => '',
                'default_value' => 0,
            ),
            array(
                'key' => create_key($name,'overlay_opacity'),
                'label' => 'Opacity',
                'name' => 'logo_block_overlay_opacity',
                'type' => 'number',
                'instructions' => 'Set from 0 to 1 (for example 0.75)',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => create_key($name,'modify'),
                            'operator' => '==',
                            'value' => '1',
                        ),
                    ),
                ),
                'wrapper' => array(
                    'width' => 35,
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'min' => 0,
                'max' => 1,
                'step' => '.01',
                'readonly' => 0,
                'disabled' => 0,
            ),

            array(
                'key' => create_key($name,'overlay_color'),
                'label' => 'Color',
                'name' => 'logo_block_overlay_color',
                'type' => 'color_picker',
                'instructions' => 'Set the color overlay for the image or video',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => create_key($name,'modify'),
                            'operator' => '==',
                            'value' => '1',
                        ),
                    ),
                ),
                'wrapper' => array(
                    'width' => 35,
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
            ),
            array(
                'key' => create_key($name,'logo'),
                'label' => 'Logo',
                'name' => 'logo_block_logo',
                'type' => 'image',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                ),
                'wrapper' => array(
                    'width' => '40%',
                    'class' => '',
                    'id' => '',
                ),
                'return_format' => 'array',
                'preview_size' => 'thumbnail',
                'library' => 'all',
                'min_width' => '',
                'min_height' => '',
                'min_size' => '',
                'max_width' => '',
                'max_height' => '',
                'max_size' => '',
                'mime_types' => '',
            ),
            
            ),
        ),
    ),
    'min' => '',
    'max' => '',
);

?>