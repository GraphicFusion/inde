<?php
	function build_action_layout(){
		global $i;
		$args = [];
		$image = mason_get_sub_field('action_block_image');
		$args['overlay_color'] = mason_get_sub_field('action_block_overlay_color');
		$args['overlay_opacity'] = mason_get_sub_field('action_block_overlay_opacity');
		$args['title'] = mason_get_sub_field('action_block_action');
		$args['img'] = mason_get_sub_field('action_block_secondary_image');
		$args['caption'] = mason_get_sub_field('action_block_content');
		$args['button'] = mason_get_sub_field('action_block_button');
		$args['module_styles'] = [];
		if( is_array( $image ) ){
			$args['bg_image_url'] = $image['url'];
		}
		if(get_sub_field('action_block_margin-top')){
			$args['module_styles']['margin-top'] = get_sub_field('action_block_margin-top');
		}
		if(get_sub_field('action_block_margin-bottom')){
			$args['module_styles']['margin-bottom'] = get_sub_field('action_block_margin-bottom');
		}
		
		if( is_array( $args ) ){
			return $args;
		}
	}

?>