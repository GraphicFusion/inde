<?php 
	global $args; 
?>
<div class=" container-fluid container-lg container-md container-sm ">
	<div class="card"  style="background-image: url(' <?php echo $args['bg_image_url']; ?>');">
		<div class="img-overlay"> 
			<div class="filter" 
			style="background-image:linear-gradient(to right, <?php echo $args['overlay_color']; ?> 100%, transparent ); opacity: <?php echo $args['overlay_opacity']; ?>;"></div>
		</div>
				<div class="action-wrapper">
					<div class="shadow-action">
						<div class="action-foot-content">
							<div class="img-wrap">
								<?php if( $args['img']  ) : ?>
									<img src="<?php echo $args['img']['url']; ?>" class="img">
								<?php endif; ?>
							</div>
							<div class="title-wrap">
								<?php if( $args['title']  ) : ?>
									<h3><?php echo $args['title']; ?></h3>
								<?php endif; ?>
								<?php if(  $args['caption']  ) : ?>
									<div class="content"><?php echo $args['caption']; ?></div>
								<?php endif; ?>
								<?php if(  $args['button']  ) :  ?>
									<a class="inde-btn" href="<?php echo $args['button']['url']; ?>"><?php echo $args['button']['title']; ?></a>
								<?php endif; ?>
							</div><!--/title-wrap-->
						</div><!--/action-foot-content-->
					</div>
				</div><!--/shadow-action-->
	</div>
</div>