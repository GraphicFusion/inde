<?php
	function build_teaser_layout(){
		global $i;
		$args = [];
		$teasers = [];
		$rows_arr = mason_get_sub_field('teaser_block_rows');
		$image = mason_get_sub_field('teaser_block_image');
		if( is_array( $image ) ){
			$args['bg_image_url'] = $image['url'];
		}
		if( is_array($rows_arr) ){
			$rows = array();
			foreach( $rows_arr as $row_arr){
				$teaser_args = array(
					'subtitle' => $row_arr['teaser_block_subtitle'],
					'title' => $row_arr['teaser_block_title'],
					'button' => $row_arr['teaser_block_link'],
					'id' => 'teaser_block'.'_'.$i,
					'content' => $row_arr['teaser_block_content'],
				);
				$teasers[] = $teaser_args;
			}
		}
		$args['teasers'] = $teasers;
		$args['module_styles'] = [];
		if(get_sub_field('teaser_block_margin-top')){
			$args['module_styles']['margin-top'] = get_sub_field('teaser_block_margin-top');
		}
		if(get_sub_field('teaser_block_margin-bottom')){
			$args['module_styles']['margin-bottom'] = get_sub_field('teaser_block_margin-bottom');
		}
		
		if( is_array( $args ) ){
			return $args;
		}
	}
	function teaser_admin_enqueue($hook) {
		wp_register_style( 'teaser_wp_admin_css', get_template_directory_uri() . '/mason-modules/teaser/admin-style.css', false, '1.0.0' );
        wp_enqueue_style( 'teaser_wp_admin_css' );
    }
	add_action( 'admin_enqueue_scripts', 'teaser_admin_enqueue' );
?>