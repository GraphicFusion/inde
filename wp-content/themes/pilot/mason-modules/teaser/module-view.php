<?php 
	global $args; 
?>
<div class="container-card container-fluid container-lg container-md container-sm ">
	<div class="card"  style="background-image: url(' <?php echo $args['bg_image_url']; ?>');">
		<div class="row">
<?php
	if(count($args['teasers']) > 0 ) :
		$i = 0;
		foreach($args['teasers'] as $teaser):  ?>
			<div class="shadow-teaser col-lg-4" >
				<div class="teaser-foot-content">
						<div class="title-wrap">
							<?php if( $teaser['title']  ) : ?>
								<h6><?php echo $teaser['title']; ?></h6>
							<?php endif; ?>
							<?php if(  $teaser['content']  ) : ?>
								<div class="content"><?php echo $teaser['content']; ?></div>
							<?php endif; ?>
							<?php if( $teaser['button']  ) : ?>
									<a class="inde-link" href="<?php echo $teaser['button']['url']; ?>" target="<?php echo $teaser['button']['target']; ?>"><?php echo $teaser['button']['title']; ?>
									</a>
							<?php endif; ?>
						</div><!--/title-wrap-->
				</div><!--/teaser-foot-content-->
			</div><!--/shadow-teaser-->
		<?php endforeach; ?>
	<?php endif; ?>
		</div>
	</div>
</div>